<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vishal extends Model
{
    protected $table = 'vishal';
    protected $fillable = ['name', 'number', 'address'];
}
