<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\vishal;


class ResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "Welcome to Crud Operations Resource Controller";
        echo "<br><br><a href='/resource/show'>Display</a>";
        echo "<br><a href='/resource/create'>Insert</a>";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        echo "<h3>Insert Page</h3>";
        return view('insert');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vishal = new vishal;
        $vishal->name = $request->name;
        $vishal->number = $request->number;
        $vishal->address = $request->address;

        $vishal->save();

        if ($vishal->save()) {
            return redirect('resource/show');
        } else {
            echo "data insertion failed";
        }


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        echo "Show/display Method";

        $data = vishal::all();

        return view('display')->with('data', $data);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $details = vishal::find($id);

        return view('edit')->with('details', $details);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vishal = vishal::find($request->id);

        $vishal->name = $request->name;
        $vishal->number = $request->number;
        $vishal->address = $request->address;

        $vishal->save();

        if ($vishal->save()) {
//            return view('display');
            return redirect()->action('ResourceController@show');

        } else {
            echo "updation failed";
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = vishal::find($id);
        $data->delete();
//        return redirect()->action('ResourceController@show');


    }

    public function res()
    {
        return "Hello";
    }
}
