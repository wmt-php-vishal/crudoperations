<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('resource', 'ResourceController');

Route::post('resource/store', 'ResourceController@store');

Route::any('/resource/delete/{id}', 'ResourceController@destroy');

Route::any('/resource/show', 'ResourceController@show');


// get method
Route::get('/about', function () {
    return "yes";
});

Route::get('res', 'ResourceController@res')->name('res_name');


Route::match(['get', 'post'], '/match-method', function () {
    echo "IF";
    die();
});

Route::view('/route-view', 'welcome', ['data' => ['name' => 'Vishal']]);

Route::get('search/{search}', function ($search) {
    dd(
        route('profile', ['id' => 2]),
        'search is '.$search
    //url('user/profile')
    );
})->where('search', '.*');


//Route::get('user/profile', function () {
//    //
//})->name('profile');


Route::get('user/{id}/profile', function ($id) {
    dd('hello');
})->name('profile');

Route::get('users/{id}', function ($id) {
    return \App\vishal::where('id', $id)->firstOrFail();
});

Route::prefix('admin')->group(function () {

    Route::namespace('Admin')->group(function () {

        Route::get('users', 'HomeController@index');
    });
});


//    Route::namespace('Admin')->group(function ()
//    {
//        Route::get('index' , 'HomeController@index', ['name' => 'vishal']);
//    });

Route::prefix('student')->group(function () {
    Route::get('mca', function () {
        echo "mca students prefix -student";
    });

    Route::get('mba', function () {
        echo "mba students - prefix student";
    });
});

//Route::get('name/{name?}', function ($name = Null) {
//    return "Name is - ".$name;
//});

Route::get('name/{name}', function ($name) {
    return "Your name is - ".$name;
})->where('name', '[A-Za-z ]+');

Route::get('student/profile', 'ResourceController@index')->name('profile');



