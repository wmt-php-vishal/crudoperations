<form action="/resource/{resource}" method="post">
    @method('PATCH')
    @csrf
    <table>
        <h3>Update Page</h3>
        <tr>
            <td>Name</td>
            <input type="text" hidden name="id" value="{{ $details->id  }}">
            <td><input type="text" name="name" value="{{$details->name}}"></td>
        </tr>
        <tr>
            <td>Number</td>
            <td><input type="text" name="number" value="{{$details->number}}"></td>
        </tr>
        <tr>
            <td>Address</td>
            <td><input type="text" name="address" value="{{$details->address}}"></td>
        </tr>
        <tr>
            <td><input type="submit" name="submit" value="Update"></td>
        </tr>
    </table>
</form>

<a href='/resource'>Home</a>


